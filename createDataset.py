from VLADlib.VLAD import *
from growing_network.utils import pca
import os
import cPickle as pickle

threads = 8

def createDataset(dataset):

    if not os.path.isfile(dataset.dictionary + '.pkl'):
        # computing the descriptors
        descriptors = getDescriptors(dataset.dataset, dataset.descriptorMethod, threads, numMaxDescriptor=dataset.numMaxDescriptor)
        print descriptors.shape

        print ("####### pca #######")
        descriptors, _, projection = pca(descriptors, 5)

        # computing the visual dictionary
        print("####### KMeans #######")
        visualDictionary = kMeansDictionary(descriptors, dataset.numberOfVisualWords)

        # output
        print("####### Saving dictionary #######")
        np.save(dataset.proj, projection)
        with open(dataset.dictionary + '.pkl', 'wb') as f:
            pickle.dump(visualDictionary, f, protocol=pickle.HIGHEST_PROTOCOL)

    print "done"
