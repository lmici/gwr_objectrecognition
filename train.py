from sklearn.metrics import accuracy_score, classification_report, confusion_matrix

from VLADlib.VLAD import *
from growing_network.GrowingWhenRequired import LabellingStrategy, GWR
import cPickle as pickle
import os

def __train(dataset, X, Y, X_test, Y_test):
    print '####### training GWR ##############'
    X = np.asarray(X)
    Y = np.asarray(Y)
    X_test = np.asarray(X_test)
    Y_test = np.asarray(Y_test)

    if not os.path.isfile(dataset.trained_network_path + '.p'):
        print '####### Training GWR network #######'
        gwr = GWR()
        gwr.initialise_network(habituation_threshold=0.1, insertion_threshold=0.95, max_nodes=15000, max_age=100)
        gwr.train_and_labeling(x=X, y=Y, max_epoch=50, random_initialization=False,
                               labeling_strategy=LabellingStrategy.online_frequency_based)
        print '####### Saving trained GWR network #######'
        gwr.save_network(dataset.trained_network_path)
    else:
        gwr = GWR.load_network(dataset.trained_network_path)

    print '####### test GWR #######'

    _, predicted_labels = gwr.classify(X_test)
    accuracy = accuracy_score(Y_test, predicted_labels)
    print('Classification accuracy on the test set: ' + str(accuracy))
    print('******************************************')
    print((classification_report(Y_test, predicted_labels)))
    print('******************************************')
    print(confusion_matrix(Y_test, predicted_labels, labels=None))
    print('******************************************')


def train_gwr(dataset):

    if not os.path.isfile(dataset.dictionary + '.pkl') or not os.path.isfile(dataset.proj + '.npy'):
        raise Exception('visual dictionary or projections not found!')

    else:
        visualDictionary = pickle.load(open(dataset.dictionary + '.pkl', "rb"))
        projections = np.load(dataset.proj + '.npy')

    if dataset.divide_train_test:
        files, labels, files_test, labels_test = dataset.create_train_test()
    else:
        files = glob.glob(dataset.datasetPath + "/**/*.png")+glob.glob(dataset.datasetPath + "/**/*.jpg")
        labels = [dataset.getLabelFromPath(f) for f in files]
        files_test = None
        labels_test = None


    print("####### estimating VLAD Train descriptors #######")
    data, images = getVLADDescriptors(files, dataset.descriptorMethod, visualDictionary, projections, threads=8)

    if dataset.divide_train_test:
        print("####### estimating VLAD Test descriptors #######")
        data_test, _ = getVLADDescriptors(files_test, dataset.descriptorMethod, visualDictionary, projections, threads=8)
        __train(dataset, data, labels, data_test, labels_test)

    else:
        __train(dataset, data, labels, data, labels)
