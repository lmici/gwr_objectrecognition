import cPickle as pickle
import numpy as np

from VLADlib.VLAD import computeSingleVladDescriptor, describePHOW
from growing_network.GrowingWhenRequired import GWR

class ObjectRecognition():
    def __init__(self, dataset):

        self.__dataset = dataset

        self.functionHandleDescriptor = describePHOW

        self.visualDictionary = pickle.load(open(self.__dataset.dictionary + '.pkl', "rb"))
        self.projections = np.load(self.__dataset.proj + '.npy')
        self.gwr = GWR.load_network(self.__dataset.trained_network_path)


    def gwrRecognition(self, imagePath):
        descs, _ = computeSingleVladDescriptor((imagePath,  self.functionHandleDescriptor,  self.visualDictionary,  self.projections))
        _, pred_label = self.gwr.classify(descs.reshape(1, len(descs)))

        for object_name, object_id in  self.__dataset.object_types.items():  # for name, age in dictionary.iteritems():  (for Python 2.x)
            if object_id == pred_label[0]:
                print(object_name)

        return pred_label[0]
