# GWR Object Recognition

Creator: Luiza Mici 

.. this work is part of my PhD

### Dependecy installation

The code is tested with python 2.7

We strongly suggest you use [anaconda](www.anaconda.org)

1) if you don't have install first [miniconda](https://conda.io/miniconda.html)

2) install requirements ``` pip install -r requirements.txt ```
3) install cyvlfeat ``` conda install -c menpo cyvlfeat ```
4) istall imageio ``` conda install -c conda-forge imageio ```
5) (optional) if you use PHOW descriptor you don't need opencv3, 
instead install opencv3 ``` conda install -c menpo opencv3 ```
6) (optional) ``` conda install -c conda-forge matplotlib ```


#### Object recognition

Create your own implementation of 
```python
 from dataset import Dataset
 
 class MyDataset(Dataset):
    def __init__(self):
        # TODO: my config based on Dataset filds
 
 ```
you can test with [caltech dataset](http://www.vision.caltech.edu/Image_Datasets/Caltech101/)

```python
 from createDataset import createDataset
 from dataset import CaltechDataset
 from train import train_gwr
 from recognition import ObjectRecognition
 
 if __name__ == '__main__':
 
     dataset = CaltechDataset()
 
     createDataset(dataset)
     train_gwr(dataset)
     
     oR = ObjectRecognition(dataset)
     
     label = oR.gwrRecognition("data/cal/image.png")
 
 ```

### References

- Marsland, S., Shapiro, J., and Nehmzow, U. (2002). A self-organising network that grows when required. Neural Networks, 15(8-9):1041-1058.
- Mici, L., Parisi, G. I., Wermter, S. A self-organizing neural network architecture for learning human-object interactions. Neurocomputing, Volume 307, pages 14--24, May 2018. 