"""
.. moduleauthor:: Luiza Mici <miciluiza@gmail.com>

"""

import math
import numpy as np

from Growing_Networks import Growing_Network
from .utils import argmin_and_second_argmin

# noinspection PyClassHasNoInit
class LabellingStrategy:
    """
    The labeling strategy to use for assigning Growing Network neurons with labels.
    The options are:

    - offline_frequency_based : The most frequent label seen after training (during the labeling phase)
    - online_relabelling: The last seen label during training
    - offline_relabelling: The last seen lable after training (during the labeling phase)
    - online_frequency_based: The most frequent label seen during training
    - offline_soft_labelling: ????
    - probability_based: the labelling based on majority vote, the label histograms for each neuron are composed of \
    class labels seen during the labeling phase
    - online_probability_based: the labelling based on majority vote, the label histograms for each neuron are composed\
    of class labels seen during training

    """
    offline_frequency_based = 0
    online_relabelling = 1
    offline_relabelling = 2
    online_frequency_based = 3
    offline_soft_labelling = 4

    probability_based = 5
    online_probability_based = 6


class GWR(Growing_Network):
    """
    Implementation of the GWR algorithm
    """
    def __init__(self):
        """
        Overrides :func:`Growing_Networks.Growing_Network.__init__
        """
        Growing_Network.__init__(self)

        self.epsilon_b = None
        self.epsilon_n = None
        self.tau_b = None
        self.tau_n = None

        self.habituation_threshold = None
        self.insertion_threshold = None
        self.max_nodes = None
        self.activity = None
        self.habituation = None

        self.max_neighbor = 4

        self.dataset_index = 0

        self.labels = {}

        self.bmu1_key = 0
        self.bmu2_key = 1

        # statistics
        self.new_node_index = []
        self.track_entropy = False
        self.entropy = [0, 0]
        self.networks_activity = []
        self.tracked_habituation = {200: []}
        self.labelling_strategy = None

    def initialise_network(self,
                           habituation_threshold=.1, insertion_threshold=.9,
                           max_nodes=900, epsilon_b=.1, tau_b=.3, epsilon_n=.01, tau_n=.1,
                           max_age=100, max_neighbor=4, track_entropy=False):
        """
        Sets the learning parameters and Initializes the network by creating the two initial nodes.

        :param habituation_threshold:
        :param insertion_threshold:
        :param max_nodes: maximum number of nodes allowed during learning
        :param epsilon_b: learning rate for the bmus
        :param tau_b: constant for the habituation decrease curve function for the bmus
        :param epsilon_n: learning rate for the bmus' neighbors
        :param tau_n: constant for the habituation decrease curve function for the bmus' neighbors
        :param max_age: maximum age for edges, when reached edges are removed
        :param max_neighbor: maximum number of neighbors for each neuron (affected by the weight updates)
        :param track_entropy: flag for keeping track of the network's entropy
        """
        self.track_entropy = track_entropy
        self.epsilon_b = epsilon_b
        self.epsilon_n = epsilon_n
        self.tau_b = tau_b
        self.tau_n = tau_n

        self.habituation_threshold = habituation_threshold
        self.insertion_threshold = insertion_threshold
        self.max_nodes = max_nodes
        self.max_neighbor = max_neighbor

        self.activity = np.zeros(2)
        self.habituation = np.ones(2)

        self.max_age = max_age

        # initialize the network with 2 nodes
        self.create_node()
        self.create_node()

        self.labels = {}

        self.nodes_counter = {k: (10 ** -20) for k in [0, 1]}

    # tested
    def remove_neurons_without_neighbors(self):
        """
        Delete all neurons with no neighbors!

        .. note::
            I don't know what happens here when all neighbors are removed but the node is connected!!!

        """
        self.node_to_remove.update([key for key in self.nodes_neighbor.keys() if not self.nodes_neighbor[key]])
        if len(self.node_to_remove) > 0:
            indices_to_remove = self.delete_nodes()
            self.weights = np.delete(self.weights, indices_to_remove, 0)
            self.activity = np.delete(self.activity, indices_to_remove, 0)
            self.habituation = np.delete(self.habituation, indices_to_remove, 0)

    # tested
    def add_neighbor(self, key_node_i, key_node_j):
        """
        Overrides :func:`Growing_Networks.Growing_Network.add_neighbor`.
        Make the two neurons *i* and *j* neighbor of each other iif the max nr of neighbors has not been exceeded.

        :param key_node_i: first neuron whose neighbors will be updated
        :param key_node_j: second neuron whose neighbors will be updated
        :type key_node_j: int
        :type key_node_i: int

        """
        if len(self.nodes_neighbor[key_node_i]) < self.max_neighbor and key_node_j not in self.nodes_neighbor[key_node_i]:
            self.nodes_neighbor[key_node_i].append(key_node_j)
        if len(self.nodes_neighbor[key_node_j]) < self.max_neighbor and key_node_i not in self.nodes_neighbor[key_node_j]:
            self.nodes_neighbor[key_node_j].append(key_node_i)

    # tested
    def adapt_bmu_weights_and_his_neighbors(self, dataset_sample, bmu1_idx, bmu1_key):
        """
        - Updates weights and decreases habituation of the best matching unit and its neighbors
        - Ages all edges with an end to the bmu1

        :param dataset_sample: a data sample
        :type dataset_sample: numpy array
        :param bmu1_idx: index of the first best matching unit
        :param bmu1_key: key of the first best matching unit (key is passed as argument to \
        avoid access to ``self.node_index_to_key``)

        """
        self.weights[bmu1_idx] += self.habituation[bmu1_idx] * self.epsilon_b * (dataset_sample -
                                                                                 self.weights[bmu1_idx])
        self.habituation[bmu1_idx] += self.tau_b * 1.05 * (1 - self.habituation[bmu1_idx]) - self.tau_b

        for n_key in self.nodes_neighbor[bmu1_key]:
            n_index = self.get_node_index(n_key)
            self.weights[n_index] += self.habituation[n_index] * self.epsilon_n * (dataset_sample -
                                                                                   self.weights[n_index])
            self.habituation[n_index] += self.tau_n * 1.05 * (1 - self.habituation[n_index]) - self.tau_n

        # Step 8: Age the edges from this bmu
        map(lambda n_key: self.increment_age(bmu1_key, n_key), self.edges[bmu1_key])

    # tested
    def add_node_and_update_edges(self, dataset_sample, bmu1_idx, bmu1_key, bmu2_key):
        """
        - Adds a new node to the network with weights ``(weights[bmu1_idx] + data_sample)/2`` .
        - Creates edges: new_node<-->bmu1 and new_node<-->bmu2, removes the edge bmu1<-->bmu2,

        :param dataset_sample: the data sample that has caused the adding of a neuron
        :param bmu1_idx: index of the first best matching unit
        :param bmu1_key: key of the first best matching unit (key is passed as argument to \
        avoid access to ``self.node_index_to_key`` )
        :param bmu2_key: key of the second best matching unit

        :return: new_node_created, new_node_key
        :rtype: bool, int
        """

        new_node_created = False
        new_node_key = None
        if self.nr_nodes < self.max_nodes:
            new_node_key = self.create_node()

            # add edge between new node and bmu1
            self.add_edge(new_node_key, bmu1_key)

            # FIXME: 18.04.2017 the following line has been added
            # add edge between new node and bmu2
            self.add_edge(new_node_key, bmu2_key)

            # remove edge between bmu1 and bmu2
            self.remove_edge(bmu1_key, bmu2_key)

            # update matrix
            self.activity = np.hstack((self.activity, np.zeros(1)))
            self.habituation = np.hstack((self.habituation, np.ones(1)))

            new_weights = (self.weights[bmu1_idx] + dataset_sample) / 2.
            self.weights = np.vstack((self.weights, new_weights))

            new_node_created = True

        return new_node_created, new_node_key

    def find_bmus(self, data_sample):
        """
        Overrides :func:`Growing_Networks.Growing_Network.find_bmus`.
        Finds the first and the second best matching unit for the given data sample.

        :param data_sample: the data sample
        :type data_sample: numpy array

        :return first bmu, second bmu, smallest distance

        """
        if self.dataset_index in self.squared_data:
            square_data = self.squared_data[self.dataset_index]
        else:
            square_data = None
        return argmin_and_second_argmin(
            self.distance_function(x=data_sample, y=self.weights, x_norm_squared=square_data))

    def compute_activity(self, node_idx, distance):
        """
        Computes activity of the node as: ``math.exp(-(distance ** 2))``

        :param node_idx: index of the node
        :type node_idx: int
        :param distance: distance with data sample
        :type distance: float
        """
        self.activity[node_idx] = math.exp(-(distance ** 2))

    def train(self, x, y=None, max_epoch=50, track_qe=False, random_initialization=True, verbose=True):
        """
        Calls :func:`Growing_Networks.Growing_Network.train` and when training is finished removes old connections
        and neurons without connections.

        :param x: the data samples
        :type x: ndarray
        :param y: the labels
        :type y: ndarray
        :param max_epoch: number of maximum epochs to run the training
        :type max_epoch: int
        :param track_qe: flag for tracking the quantization error of bmus during each learning iteration
        :type track_qe: bool
        :param random_initialization: flag for the random initialization of the weights
        :type random_initialization: bool
        :param verbose: flag for verbosity of this method
        :type verbose: bool

        """
        Growing_Network.train(self, x, y, max_epoch, track_qe, random_initialization, verbose)
        # final clean
        self.remove_old_connection()
        self.remove_neurons_without_neighbors()

    def one_step_train(self, dataset_sample, label, t):
        """
        Implements :func:`Growing_Networks.Growing_Network.one_step_train`.

        - Finds first and second best matching units
        - Adds an edge between them
        - Computes activity of the first best matching unit bmu1
        - If habituation[bmu1] is lower than the habituation threshold and activity[bmu1] is lower than the \
        insertion_threshold adds a neuron, otherwise trains the bmu1 and its neighbors


        :param dataset_sample: a 1D vector
        :type dataset_sample: numpy array
        :param label: label of the current data sample
        :param t: training iteration
        """
        self.dataset_index = t

        # Step 2, 3: Find the best and second-best matching neurons
        bmu1_idx, bmu2_idx, distance = self.find_bmus(dataset_sample)
        distance = math.sqrt(abs(distance))
        if self.track_qe:
            self.quantization_error.append(distance)

        bmu1_key = self.get_node_key(bmu1_idx)
        bmu2_key = self.get_node_key(bmu2_idx)

        self.bmu1_key = bmu1_key
        self.bmu2_key = bmu2_key

        # Step 4: add edge between best matching units
        self.add_edge(bmu1_key, bmu2_key)

        # Step 5: calculate the activity of the best matching unit
        self.compute_activity(bmu1_idx, distance)
        if self.track_qe:
            self.networks_activity.append(self.activity[bmu1_idx])

        # NOTE: Network habituation for certain nodes
        # For debugging purposes enable the following 3 command lines
        # for kk in self.tracked_habituation.keys():
        #     if kk in self.key_to_node_index:
        #         self.tracked_habituation[kk].append(self.habituation[self.get_node_index(kk)])

        if self.track_entropy:
            if bmu1_key in self.nodes_counter:
                self.nodes_counter[bmu1_key] += 1
            else:
                self.nodes_counter[bmu1_key] = 1

            # calculate entropy
            e = np.array([self.nodes_counter[key] / np.float(len(self.entropy) - 1)
                          for key in self.nodes_counter.keys()])
            entropy = -1 * np.sum(e * (np.log(e)))
            if np.isnan(entropy):
                print 'Computed entropy is nan'
            self.entropy.append(entropy)

        # if if habituation[bmu1] < self.habituation_threshold and activity[bmu1] < self.insertion_threshold
        if self.habituation[bmu1_idx] < self.habituation_threshold \
                and self.activity[bmu1_idx] < self.insertion_threshold\
                and self.max_nodes > self.nr_nodes:

            self.new_node_index.append(self.iteration_index)
            # Step 6 Add new node and update edges
            _, new_key_node = self.add_node_and_update_edges(dataset_sample, bmu1_idx, bmu1_key, bmu2_key)

            # Step 6.5 set label if label strategy = online
            self.set_label_for_node(new_key_node, label)

        else:
            # Step 7: If no new nodes, adapt weights and habituate for bmu1 and his neighbors, then age edges
            self.adapt_bmu_weights_and_his_neighbors(dataset_sample, bmu1_idx, bmu1_key)

            # Step 8 set label if label strategy = online
            self.set_label_for_node(bmu1_key, label)

            # Step 9: Remove old connections
            self.remove_old_connection()

        if self.squared_data is None or t == self.last_dataset_index or self.clean_network or t % 1 == 0:
            # Step 10: remove neurons without connections
            self.remove_neurons_without_neighbors()

        self.dataset_index = -1

    def train_and_labeling(self, x, y, max_epoch, track_qe=False, random_initialization=True,
                           labeling_strategy=LabellingStrategy.online_frequency_based, verbose=True):
        """
        A wrapping function that calls :func:`GrowingWhenRequired.train` and when training is finished
        computes the labels for each network unit according to the labeling strategy (this is of course done only for
        batch learning).


        :param x: the data samples
        :type x: ndarray
        :param y: the labels
        :type y: ndarray
        :param max_epoch: number of maximum epochs to run the training
        :type max_epoch: int
        :param track_qe: flag for tracking the quantization error of bmus during each learning iteration
        :type track_qe: bool
        :param random_initialization: flag for the random initialization of the weights
        :type random_initialization: bool
        :param labeling_strategy: the strategy to use for neuron labelling \
        (options are defined in :class:`GrowingWhenRequired.LabellingStrategy`
        :param verbose: flag for verbosity of this method
        :type verbose: bool

        """
        self.labelling_strategy = labeling_strategy
        self.train(x, y, max_epoch, track_qe, random_initialization=random_initialization, verbose=verbose)

        if labeling_strategy == LabellingStrategy.offline_frequency_based:
            self.labeling(x, y)

        if labeling_strategy == LabellingStrategy.probability_based \
                or labeling_strategy == LabellingStrategy.online_probability_based:
            self.labeling_probability_based(x, y)

    def labeling(self, x, y):
        self.labels = {key: [] for key in self.nodes_keys}
        for i in xrange(len(x)):
            data_sample = x[i]
            label = y[i]
            bmu_idx, _, distance = self.find_bmus(data_sample)
            self.labels[self.get_node_key(bmu_idx)].append(label)

    def get_labels(self):

        """
        Returns neurons' labels which are computed according to the labelling strategy.
        If a neuron has no label, -1 is returned.

        :rtype: dict
        """

        import scipy.stats as scp
        _labels = {key: -1 for key in self.nodes_keys}

        for key in _labels.keys():

            try:
                values = self.labels[key]
            except:
                values = []
                if self.labelling_strategy is LabellingStrategy.online_frequency_based \
                        or self.labelling_strategy is LabellingStrategy.online_relabelling\
                        or self.labelling_strategy is LabellingStrategy.offline_frequency_based\
                        or self.labelling_strategy is LabellingStrategy.offline_soft_labelling:
                    print "labels for node key " + str(key) + " not found!"

            if len(values) > 0:
                if self.labelling_strategy is LabellingStrategy.offline_frequency_based \
                        or self.labelling_strategy is LabellingStrategy.online_frequency_based \
                        or self.labelling_strategy is LabellingStrategy.online_probability_based:
                    _labels[key] = scp.mode(values)[0][0]

                elif self.labelling_strategy is LabellingStrategy.offline_relabelling \
                        or self.labelling_strategy is LabellingStrategy.online_relabelling:
                    _labels[key] = values[-1]
                elif self.labelling_strategy is LabellingStrategy.offline_soft_labelling:
                    _labels[key] = np.argmax(self.unit_class_histograms[key])

        return _labels

    def get_num_classes(self):
        # FIXME: This method doesn't work when labelling is set to probability based!
        return np.max(self.get_labels().values())

    def classify(self, x, with_distance=False):
        """
        Finds distances between each data sample in x with the nodes in the network and
        returns best matching units as well as the labels assigned to each bmu.

        :param x: the data samples
        :param with_distance: flag if distances should be returned as well
        :type with_distance: bool

        :return: bmus, labels
        """
        output_labels = []
        bmus = []
        distances = []
        labels = self.get_labels()

        for i in xrange(len(x)):

            data_sample = x[i]
            if ~np.isfinite(data_sample[0]):
                continue

            bmu_idx, _, distance = self.find_bmus(data_sample)
            bmu_key = self.get_node_key(bmu_idx)
            bmus.append(bmu_idx)
            distances.append(distance)
            if self.labelling_strategy < LabellingStrategy.probability_based:
                output_labels.append(int(labels[bmu_key]))
            else:
                output_labels.append(self.unit_class_histograms[bmu_idx])

        if self.labelling_strategy >= LabellingStrategy.probability_based:
            output_labels = np.argmax(output_labels, axis=1)

        if with_distance:
            return bmus, output_labels, distances

        return bmus, output_labels

    def classify_raw(self, data_sample):
        """
        Returns the activity of the BMU for the given data_sample.

        :param data_sample:
        """
        bmu_idx, _, distance = self.find_bmus(data_sample)
        # return self.weights[bmu_idx]
        # return np.array(self.labels[self.get_node_key(bmu_idx)])
        return self.activity[bmu_idx]
        # return bmu_idx

    # tested
    def get_bmus_for_dataset(self, dataset):
        """
        Computes the best matching units for all data samples in the given dataset

        :param dataset: the data samples
        :rtype: list

        """
        return [self.find_bmus(d)[0] for d in dataset if np.isfinite(d[0])]

    def plot_entropy(self):
        """
        Plots network's entropy over iterations compared to the upper bound entropy.

        :raises: Exception("Network entropy not tracked, please call train with track_entropy = True!")

        """
        if not self.track_entropy:
            raise Exception("Network entropy not tracked, please call train with track_entropy = True!")
        import matplotlib.pyplot as plt
        upper_bound_entropy = -1.0 * np.log(1.0 / np.array(self.nr_nodes))
        fig = plt.figure()
        fig.suptitle("Entropy over iterations.")
        plt.plot(self.entropy)
        plt.plot(np.ones(len(self.entropy)) * upper_bound_entropy)
        plt.show()

    def plot_networks_activity(self):
        """
        Plots bmus' activities over learning iterations.

        :raises: Exception("Network activity not tracked, please call train with track_qe = True!")
        """
        if not self.track_qe or not self.networks_activity:
            raise Exception("Network activity not tracked, please call train with track_qe = True!")
        import matplotlib.pyplot as plt
        fig = plt.figure()
        fig.suptitle("Network's activity over epochs.")
        plt.plot(self.networks_activity)
        plt.scatter(x=self.new_node_index, y=self.insertion_threshold * np.ones(len(self.new_node_index)), s=60)
        plt.show()

    def plot_neuron_habituation(self):
        """
        Plots habituation of a specific neuron that has been tracked during training.

        :raises: Exception("Neurons habituation is not tracked, please enable it!")
        """
        if self.tracked_habituation is not {}:
            raise Exception("Neurons habituation is not tracked, please enable it!")
        import matplotlib.pyplot as plt
        fig = plt.figure()
        fig.suptitle("Tracked habituation")
        for key in self.tracked_habituation.keys():
            plt.plot(self.tracked_habituation[key])

        plt.show()

    # noinspection PyUnresolvedReferences
    def plot_graph(self, colors, node_labels, classes, weights=None, with_cmap=False):

        """
        Plots a 3-dimensional graph, where nodes have coordinates of weights and color and label specified in the args.
        Connection between nodes is defined in self.edges. Colors of the nodes can be class related or they represent
        the activity of the network corresponding to a specific input vector.

        :param with_cmap:
        :param colors: numpy array of colors associated to each node
        :param node_labels: numpy array of labels associated to each node
        :param weights: if None the matrix self.weights is used
        :param classes:
        """
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D

        if weights is None:
            weights = self.weights

        fig = plt.figure()
        ax = fig.gca(projection='3d')

        for j in xrange(len(classes)):

            class_indexes = (node_labels == classes[j]).reshape(-1)
            _weights_for_class = weights[class_indexes]
            _colors_for_class = colors[class_indexes]
            if with_cmap:
                ax.scatter(_weights_for_class[:, 0], _weights_for_class[:, 1], _weights_for_class[:, 2],
                           s=300, c=_colors_for_class[:, 0], label=str(classes[j]), cmap='Oranges')
            else:
                ax.scatter(_weights_for_class[:, 0], _weights_for_class[:, 1], _weights_for_class[:, 2],
                           s=300, c=_colors_for_class, label=str(classes[j]))

        ax.legend()

        for key, values in self.edges.iteritems():
            for value in values:
                node1 = self.get_node_index(key)
                node2 = self.get_node_index(value)
                ax.plot([weights[node1][0], weights[node2][0]],
                        [weights[node1][1], weights[node2][1]],
                        [weights[node1][2], weights[node2][2]], linewidth=1.0, color='gray')

        ax.set_xlabel('PC1')
        ax.set_ylabel('PC2')
        ax.set_zlabel('PC3')

        plt.show()

    def plot_graph_2d(self, fig=None):
        import matplotlib.pyplot as plt

        # weights = self.get_pca_weights(pc=2)
        weights = self.weights

        if fig is None:
            fig = plt.figure()

        ax = fig.gca()
        ax.scatter(weights[:, 0], weights[:, 1], color='black')

        for key, values in self.edges.iteritems():
            for value in values:
                node1 = self.get_node_index(key)
                node2 = self.get_node_index(value)
                ax.plot([weights[node1][0], weights[node2][0]],
                        [weights[node1][1], weights[node2][1]], linewidth=1.0, color='gray')

        ax.set_xlabel('X1', fontsize=20)
        ax.set_ylabel('X2', fontsize=20)

        plt.show()

    def plot_graph_2d_with_labels(self, colors, node_labels, classes, weights=None, with_cmap=False, target_names=None):

        """
        Plots a 2-dimensional graph, where nodes have coordinates of weights and color and label specified in the args.
        Connection between nodes is defined in self.edges. Colors of the nodes can be class related or they represent
        the activity of the network corresponding to a specific input vector.

        :param with_cmap:
        :param colors: numpy array of colors associated to each node
        :param node_labels: numpy array of labels associated to each node
        :param weights: if None the matrix self.weights is used
        :param classes:
        """

        import matplotlib.pyplot as plt

        if weights is None:
            weights = self.weights

        font = {'family': 'Times New Roman', 'size': 18}

        plt.rc('font', **font)

        fig = plt.figure()
        ax = fig.gca()

        if target_names is None:
            target_names = ['biscuitsbox', 'can', 'mug', 'phone']
        for j in xrange(len(classes)):

            # class_indexes = (node_labels == classes[j]).reshape(-1)
            class_indexes = [i for i, x in enumerate(node_labels) if x == classes[j]]
            _weights_for_class = weights[class_indexes]
            _colors_for_class = colors[class_indexes]
            if with_cmap:
                ax.scatter(_weights_for_class[:, 0], _weights_for_class[:, 1],
                           s=50, c=_colors_for_class[:, 0], label=str(classes[j]), cmap='Oranges')
            else:
                ax.scatter(_weights_for_class[:, 0], _weights_for_class[:, 1],
                           s=50, c=_colors_for_class, label=str(target_names[classes[j]]))

        ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=4, borderaxespad=0.)

        # for key, values in self.edges.iteritems():
        #     for value in values:
        #         node1 = self.get_node_index(key)
        #         node2 = self.get_node_index(value)
        #         ax.plot([weights[node1][0], weights[node2][0]],
        #                 [weights[node1][1], weights[node2][1]], linewidth=1.0, color='gray')

        ax.set_xlabel('PC1', fontsize=20)
        ax.set_ylabel('PC2', fontsize=20)

        plt.show()

    def plot_clusters(self):

        import matplotlib.cm as cm

        labels = self.get_labels()
        for node, label in labels.iteritems():
            if np.array(label).ndim == 1:
                labels[node] = np.argmax(label)

        unique_labels = np.unique(labels.values())
        if len(unique_labels) < max(unique_labels) + 1:
            classes = np.arange(0, max(unique_labels)+1)
        else:
            classes = unique_labels

        random_colors = cm.rainbow(np.linspace(0, 1, max(classes)+2))

        # sort labels to match indexes of self.weights matrix
        sorted_labels = np.empty((self.weights.shape[0], 1))
        colors = np.empty((sorted_labels.shape[0], 4))
        for i in xrange(len(self.weights)):
            _label = labels[self.get_node_key(i)]
            sorted_labels[i] = _label
            colors[i] = random_colors[np.where(classes == _label)]

        self.plot_graph_2d_with_labels(colors, sorted_labels, classes, self.get_pca_weights(pc=2), target_names=classes)

    def plot_clusters_lda(self, target_names=None):
        import matplotlib.pyplot as plt

        import matplotlib.cm as cm
        from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

        labels = self.get_labels()
        for node, label in labels.iteritems():
            if np.array(label).ndim == 1:
                labels[node] = np.argmax(label)

        unique_labels = np.unique(labels.values())
        if len(unique_labels) < max(unique_labels) + 1:
            classes = np.arange(0, max(unique_labels) + 1)
        else:
            classes = unique_labels

        if target_names is None:
            target_names = classes

        random_colors = cm.rainbow(np.linspace(0, 1, max(classes) + 1))
        markers = ['*', 'o', 'v', 'p', 'h', 'x', 'D', 's', '^', '1']

        X = self.weights
        Y = labels.values()

        lda = LinearDiscriminantAnalysis(n_components=2)
        X_r2 = lda.fit(X, Y).transform(X)

        # plt.figure()
        for color, i, target, marker in zip(random_colors, np.arange(len(random_colors)), target_names, markers):
            plt.scatter(X_r2[Y == i, 0], X_r2[Y == i, 1], alpha=.8, color=color, label=target, marker=marker, s=60)
        plt.legend(loc='best', shadow=False, scatterpoints=1)
        plt.title('LDA')

        plt.xlabel('LDA proj. axis 1')
        plt.ylabel('LDA proj. axis 2')

        plt.tight_layout()
        plt.show()

    def plot_activity(self, data_sample):

        import matplotlib.pyplot as plt

        labels = self.get_labels()
        classes = np.unique(labels.values())

        distances = self.distance_function(data_sample, self.weights)

        # sort labels to match indexes of self.weights matrix
        sorted_labels = np.empty((self.weights.shape[0], 1))
        colors = np.empty((sorted_labels.shape[0], 3))
        for i in xrange(len(self.weights)):
            _label = labels[self.get_node_key(i)]
            sorted_labels[i] = _label
            colors[i] = distances[i]

        self.plot_graph_2d_with_labels(colors, sorted_labels, classes, self.get_pca_weights(pc=2), with_cmap=True)
        plt.show()

    def set_label_for_node(self, key, label):
        if (self.labelling_strategy == LabellingStrategy.online_frequency_based
            or self.labelling_strategy == LabellingStrategy.online_relabelling
            or self.labelling_strategy == LabellingStrategy.online_probability_based) \
                and label is not None:

            # if len(np.shape(label)) != 0:
            #     l = np.argmax(label)
            # else:
            #     l = label

            l = label
            if key not in self.labels:
                self.labels[key] = [l]
            else:
                self.labels[key].append(l)

    # tested
    def labeling_probability_based(self, x, y):
        print "Probability based Labeling..."

        # Remove end of sequence markers to count nr of classes and class frequencies
        labels = y
        class_counts = np.bincount(np.asarray(labels, dtype=int))
        class_labels = np.arange(np.amax(labels) + 1)

        # Calculate class frequency
        class_frequency = class_counts / float(len(labels))
        # To prevent division by zero
        class_frequency[class_frequency == 0] = 10 ** -20

        self.unit_class_histograms = {key: [] for key in np.arange(self.nr_nodes)}
        data_length, data_dimension = np.shape(x)

        activation_counter = np.zeros(self.nr_nodes, dtype=np.float64)

        if self.labelling_strategy == LabellingStrategy.online_probability_based:
            for key in self.nodes_keys:
                if key in self.labels:
                    online_label_for_bmu = self.labels[key]
                    bmu_idx = self.get_node_index(key)
                    activation_counter[bmu_idx] += 1
                    self.unit_class_histograms[bmu_idx].extend(online_label_for_bmu)
                else:
                    print "label not found for node:", key
                    print self.edges[key], self.nodes_neighbor[key]

        else:
            for i in xrange(data_length):
                data_sample = x[i]
                if np.isinf(data_sample[0]).all():
                    continue
                bmu_idx, _, distance = self.find_bmus(data_sample)
                activation_counter[bmu_idx] += 1
                self.unit_class_histograms[bmu_idx].append(y[i])

        neuron_activation_frequency = activation_counter / (1.0 * len(labels))
        neuron_activation_frequency[neuron_activation_frequency == 0] = 10 ** -20  # To prevent division by zero

        for key in self.unit_class_histograms.keys():
            his = np.histogram(self.unit_class_histograms[key], bins=np.arange(np.max(class_labels) + 2))[0]
            # maybe multiplied by neuron activation frequency and not the inverse (?).
            self.unit_class_histograms[key] = his * (1.0 / class_frequency) * (1.0 / neuron_activation_frequency[key])
