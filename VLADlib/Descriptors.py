import numpy as np

from .vl_phow import phow


def describeSURF(image):
    # doc at http://docs.opencv.org/master/d5/df7/classcv_1_1xfeatures2d_1_1SURF.html#gsc.tab=0
    from cv2 import xfeatures2d
    surf = xfeatures2d.SURF_create()
    # it is better to have this value between 300 and 500
    surf.setHessianThreshold(400)
    kp, des = surf.detectAndCompute(image, None)
    return kp, des


def describeSIFT(image):
    # doc at http://docs.opencv.org/master/d5/d3c/classcv_1_1xfeatures2d_1_1SIFT.html#gsc.tab=0
    # draw keypoints
    from cv2 import xfeatures2d
    sift = xfeatures2d.SIFT_create()
    kp, des = sift.detectAndCompute(image, None)
    return kp, des


def describeORB(image):
    # An efficient alternative to SIFT or SURF
    # doc http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_orb/py_orb.html
    # ORB is basically a fusion of FAST keypoint detector and BRIEF descriptor
    # with many modifications to enhance the performance
    from cv2 import ORB_create
    orb = ORB_create()
    kp, des = orb.detectAndCompute(image, None)
    return kp, des


def describePHOW(image, color='rgb'):
    return phow(np.array(image, np.float) /255.0, color=color)
