import os

import glob2 as glob
from sklearn.model_selection import train_test_split

from VLADlib.Descriptors import describePHOW


class Dataset():

    def __init__(self):
        self.object_types = {}

        self.datasetPath = None
        self.dataset = self.datasetPath

        self.divide_train_test = True

        self.dictionary = None
        self.proj = None
        self.encodedImages = None
        self.labelsImages = None
        self.trained_network_path = None

        self.descriptorMethod = describePHOW
        self.numberOfVisualWords = 60
        self.numMaxDescriptor = 60 * 800

    def getLabelFromPath(self, pathString):
        pass

    def create_train_test(self):
        pass


class PlushDataset(Dataset):

    def __init__(self):
        Dataset.__init__(self)

        self.object_types = {"banana": 0,
                             "watermelon": 1,
                             "mushroom": 2,
                             "zucchini": 3}

        self.datasetPath = "data/all/"
        self.dataset = self.datasetPath

        self.divide_train_test = False

        self.dictionary = "/tmp/dictionary_plush"
        self.proj = "/tmp/projection_plush"
        self.encodedImages = "/tmp/data_plush"
        self.labelsImages = "/tmp/labels_plush"
        self.trained_network_path = "/tmp/gwr_plush"

        self.descriptorMethod = describePHOW
        self.numberOfVisualWords = 60
        self.numMaxDescriptor = 60 * 800

    def getLabelFromPath(self, pathString):
        return self.object_types[os.path.basename(os.path.dirname(pathString)).lower()]

    def create_train_test(self):
        # implement your own method

        print("####### divide train/test #######")
        bananas = glob.glob(self.datasetPath + "Banana/*.png")
        b, b_t, bl, bl_t = train_test_split(bananas, [self.object_types['banana'] for i in xrange(len(bananas))])

        mushroom = glob.glob(self.datasetPath + "Mushroom/*.png")
        m, m_t, ml, ml_t = train_test_split(mushroom, [self.object_types['mushroom'] for i in xrange(len(mushroom))])

        watermelon = glob.glob(self.datasetPath + "Watermelon/*.png")
        w, w_t, wl, wl_t = train_test_split(watermelon,
                                            [self.object_types['watermelon'] for i in xrange(len(watermelon))])

        zucchini = glob.glob(self.datasetPath + "Zucchini/*.png")
        z, z_t, zl, zl_t = train_test_split(zucchini, [self.object_types['zucchini'] for i in xrange(len(zucchini))])

        files = b + m + w + z
        labels = bl + ml + wl + zl

        files_test = b_t + m_t + w_t + z_t
        labels_test = bl_t + ml_t + wl_t + zl_t

        return files, labels, files_test, labels_test


class CaltechDataset(Dataset):

    def __init__(self):
        Dataset.__init__(self)

        self.datasetPath = "data/caltech101/101_ObjectCategories/"
        self.dataset = self.datasetPath

        self.object_types = {os.path.basename(os.path.dirname(pa)) for pa in (glob.glob(self.datasetPath + "**/*.jpg"))}
        self.object_types_keys = sorted([k for k in self.object_types])
        self.object_types = {v: k for k, v in enumerate(self.object_types_keys)}

        self.divide_train_test = True

        self.dictionary = "/tmp/dictionary_caltech"
        self.proj = "/tmp/projection_caltech"
        self.encodedImages = "/tmp/data_caltech"
        self.labelsImages = "/tmp/labels_caltech"
        self.trained_network_path = "/tmp/gwr_caltech"

        self.descriptorMethod = describePHOW
        self.numberOfVisualWords = 60
        self.numMaxDescriptor = 60 * 800

    def getLabelFromPath(self, pathString):
        return self.object_types[os.path.basename(os.path.dirname(pathString))]

    def create_train_test(self):
        # implement your own method

        files, labels, files_test, labels_test = list(), list(), list(), list()

        for k in self.object_types_keys:
            f = glob.glob(self.datasetPath + k + "/**/*.jpg")
            x, x_t, y, y_t = train_test_split(f, [self.object_types[k] for i in xrange(len(f))])

            files.extend(x)
            labels.extend(y)
            files_test.extend(x_t)
            labels_test.extend(y_t)

        return files, labels, files_test, labels_test
