from createDataset import createDataset, getSingleImageDescriptor, describePHOW
from dataset import PlushDataset, CaltechDataset
from recognition import ObjectRecognition
from train import train_gwr

if __name__ == '__main__':

    dataset = PlushDataset()

    #createDataset(dataset)
    #train_gwr(dataset)
    oR = ObjectRecognition(dataset)
    oR.gwrRecognition('/tmp/mushroom.png')
